// const myVariable = "Hello again";
// console.log("myVariable");

let myVariable1

let firstName = "Isha";
let lastName= "Rullan";
let pokemon = 2500

let product_description = "sample";

let productName = "desktop computer";
console.log(productName);

let productPrice = 18999;
console.log(productPrice);

const interest = 3.539;

productName = "Laptop";
console.log(productName);

let friend = "Tracey";
let anotherFriend = "Japhet";
console.log(friend);

const pi = 3.14;
console.log(pi);

let supplier;

supplier = "John Smith Tradings";
console.log(supplier);

supplier = "Zuitt Store";
console.log(supplier); 

// let a;
a = 5;
console.log(a);
var a;

// global scope
const outerVariable = "hello";

{
	// block/local scope = {}
const innerVariable = "hello again";
console.log(outerVariable);
}

// console.log(innerVariable); //inner is not defined

// Multiple Variable declarations

let productCode = 'DC017';
const productBrand = 'Dell';
console.log(productCode, productBrand);


// data types
// strings

let country = "Philippines";
let province = "Manila";
console.log(typeof country);
console.log(typeof province);

//concatenating  strings
let fullAddress = province + ', ' + country;
console.log(fullAddress)

let greetings = 'I live in the' + country;
console.log(greetings);


console.log(`I live in the ${province}, ${country}`);

// escape character \n 

let mailAddress = 'Metro Manila \n\nPhilippines';
console.log(mailAddress);


let message = 'John\'s employees went home early';
console.log(message);

message = "John's employees went home early";
console.log(message);


console.log(

	`Metro Manila

	"Hello"
	'Hello'

	Philippines ${province}`
	)


// numbers
let headCount = 23;
console.log(typeof headCount);

// decimal/fractions
let grade =98.7;
console.log(typeof grade);

// exponential
let planetDistance = 2e10;
console.log(planetDistance);

console.log("John's grade last quarter is " + grade);
console.log(`John's grade last quarter is ${grade}`);

// boolean

let isMarried = false;
let inGoodConduct = true;
console.log(typeof isMarried);
console.log(typeof inGoodConduct);

// similar data types
let grades = [98.7, 92.1, 90.2, 94.6];
console.log(grades);
console.log(typeof grades);

// different data types
let details = ["John", "Smith", 32, true];
console.log(details);


// indexing
console.log(grades[1], grades[0]);

// // reassigning array elements
// let anime = ['one piece', 'one punch man', 'attack on titan']
// console.log(anime);
// anime[0] = 'kimetsu no yaiba'
// console.log(anime);

// using const

const anime = ['one piece', 'one punch man', 'attack on titan']
console.log(anime);
anime[0] = 'kimetsu no yaiba'
console.log(anime);

// object
let objectGrades = {
	firstQuarter: 98.7,
	secondQuarter: 92.1,
	thirdQuarter: 90.2,
	fourthQuarter: 84.6
};

let person = {
	fullName: 'Juan Dela Cruz',
	age: 25,
	isMarried: false,
	contact: ['0919234557890', '8123 4567'],
	address: {
		houseNumber: '345',
		city: 'Manila'
	}
}

console.log(`${person.fullName} + ${person.isMarried}`);
console.log(person.age);

console.log(person.contact[0])
console.log(person.address.city)

let firstName1 = "Dave", lastName1 = "Martinez"
console.log(firstName1,lastName1)

let fullName = firstName1 + " " + lastName1
console.log(fullName)

let word1 = "is a programmer";
let sentence = `${fullName} ${word1}`;

console.log(sentence)

//NUMBER

let numb1 = 5;
let numb2 = 6;
let numb3 = 5.5;
let numb4 = .5;

let numString1 = "5";
let numString2 = "6";

console.log(numString1 + numString2)/
console.log(numb1 + numb2) 

//TYPE COERCION/FORCED COERCION
//is the automatic or implicit conversion of values from one data type to another
console.log(numString1 + numb1) 

//Mathematical Operations (+, -, *, /, %(modulo-remainder))


//Modulo %
console.log(numb1 % numb2);
console.log(numb2 % numb1);

console.log(numb2 - numb1);

let product = 'The answer is ' + numb1 * numb2;
console.log(product)


//NULL
//It is used to intentionally express the absence of a value in a variable declaration/initialization
let girlfriend = null;
console.log(girlfriend)

let myNumber = 0;
let myString = '';
console.log(myNumber);
console.log(myString);


//UNDEFINED
//Represents the state of a variable that has been declared but without an assigned value.
let sampleVariable;
console.log(sampleVariable); 


//Anonymous function - a function without a name.
let variableFunction = function() {
	console.log("Hello Again")
}


let funcExpression = function func_name() {
	console.log("Hello!")
}

//FUNCTION INVOCATION

function declaredFunction() {
	console.log("Hello World")
}

declaredFunction();


//How about the function expression?
//They are always invoked/called using the variable name.

variableFunction();


//Function scoping

//JS scope
//global and local scope

//
//var faveCharacter = "Nezuko-chan";//global scope

function myFunction() {
	let nickName = "Isha"; //function scope
	console.log(nickName);

}


//Variables defined inside a function are not accessible (visible) from outside the function
//var/let/const are quite similar when declared inside a function
//Variables declared globally (outside of the function) have a global scope or it can access anywhere.


function showSum() {
	console.log(6 + 6)
}

showSum();


//PARAMETERS AND ARGUMENTS
//"name" is called a parameter
//parameter => acts as a named variable/container that exists ONLY inside of the function. Usually parameter is the placeholder of an actual value
let name = "Isha Rullan";


function printName(pikachu) {
		console.log(`My name is ${pikachu}`);
	};


//argument is the actual value/data that we passed to our function
//We can also use a variable as an argument
printName(name);

//Multiple Parameters and Argumments


function displayFullName(fName, lName, age) {
	console.log(`${fName} ${lName} is ${age}`)
}



displayFullName("Isha", "Rullan", 25);
//Providing less arguments than the expected parameters will automatically assign an undefined value to the paramter.
//Providing more arguments will not affect our function.


//return keyword
	//The "return" statement allows the ouptut of a function to be passed to the block of code that invoked the function
	//any line/block of code that comes after the return statement is ignored because it ends the function execution

	//return keyword is used so that a function may return a value.
	//after returning the value, the next statement will stop its process

	function createFullName(firstName, middleName, lastName) {
		return `${firstName} ${middleName} ${lastName}`
		console.log("I will no longer run because the function's value/result has been returned")
	}

	//result of this function(return) can be saved into a variable

	let fullName1 = createFullName("Tom", "Cruise", "Mapother");
	console.log(fullName1);

	//The result of a function without a return keyword will not save the result in a variable;

	let fullName2 = displayFullName("William", "Bradley", "Pitt");
	console.log(fullName2);


	let fullName3 = createFullName("Jeffrey", "John", "Smith");
	console.log(fullName3);


/*
Mini Activity
	Create a function can receive two numbers as arguments.
		-display the quotient of the two numbers in the console.
		-return the value of the division

	Create a variable called quotient.
		-This variable should be able to receive the result of division function

	Log the quotient variable's value in the console with the following message:

	"The result of the division is: <value of Quotient>"
		-use concatenation/template literals

	Take a screenshot of your console and send it in the hangouts.

*/

// //Stretch goals
// //get the number with the use of prompt
// prompt("What is your name")


function miniActivity(number1, number2){
	// return `${number1}` / `${number2}`;
	return(number1/number2);
};

let number1_input = prompt("Please enter your first number:", "");
let number2_input = prompt("Please enter your second number:", "");

let quotient = miniActivity(number1_input, number2_input);
console.log("The result of the division is " + quotient);

// Function as an argument
function argumentFunction (){
	console.log("This function was passed as an argument before the message was printed")
}

function invokeFunction(argumentFunction){
	argumentFunction();
}

invokeFunction(argumentFunction)





































